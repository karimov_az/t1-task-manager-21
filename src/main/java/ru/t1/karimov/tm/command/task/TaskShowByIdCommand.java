package ru.t1.karimov.tm.command.task;

import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.model.Task;
import ru.t1.karimov.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    @Override
    public void execute() throws AbstractException {
        final String userId = getUserId();
        System.out.println("[TASK LIST BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = getTaskService().findOneById(userId, id);
        showTask(task);
    }

    @Override
    public String getName() {
        return "task-show-by-id";
    }

    @Override
    public String getDescription() {
        return "Show task by id.";
    }

}
