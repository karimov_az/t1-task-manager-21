package ru.t1.karimov.tm.command.task;

import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.util.TerminalUtil;

public final class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @Override
    public void execute() throws AbstractException {
        final String userId = getUserId();
        System.out.println("[UPDATE TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        getTaskService().updateById(userId, id, name, description);
    }

    @Override
    public String getName() {
        return "task-update-by-id";
    }

    @Override
    public String getDescription() {
        return "Update task by id.";
    }

}
