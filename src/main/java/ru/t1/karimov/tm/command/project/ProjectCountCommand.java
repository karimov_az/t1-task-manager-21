package ru.t1.karimov.tm.command.project;

import ru.t1.karimov.tm.exception.AbstractException;

public final class ProjectCountCommand extends AbstractProjectCommand {

    @Override
    public void execute() throws AbstractException {
        final String userId = getUserId();
        System.out.println("[PROJECT COUNT]");
        System.out.println("Count = " + getProjectService().getSize(userId));
    }

    @Override
    public String getName() {
        return "project-count";
    }

    @Override
    public String getDescription() {
        return "Count projects.";
    }

}
