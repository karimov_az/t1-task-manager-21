package ru.t1.karimov.tm.component;

import ru.t1.karimov.tm.api.repository.ICommandRepository;
import ru.t1.karimov.tm.api.repository.IProjectRepository;
import ru.t1.karimov.tm.api.repository.ITaskRepository;
import ru.t1.karimov.tm.api.repository.IUserRepository;
import ru.t1.karimov.tm.api.service.*;
import ru.t1.karimov.tm.command.AbstractCommand;
import ru.t1.karimov.tm.command.project.*;
import ru.t1.karimov.tm.command.system.*;
import ru.t1.karimov.tm.command.task.*;
import ru.t1.karimov.tm.command.user.*;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.karimov.tm.exception.system.CommandNotSupportedException;
import ru.t1.karimov.tm.model.Project;
import ru.t1.karimov.tm.model.Task;
import ru.t1.karimov.tm.repository.CommandRepository;
import ru.t1.karimov.tm.repository.ProjectRepository;
import ru.t1.karimov.tm.repository.TaskRepository;
import ru.t1.karimov.tm.repository.UserRepository;
import ru.t1.karimov.tm.service.*;
import ru.t1.karimov.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ILoggerService loggerService = new LoggerService();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    {
        registry(new ApplicationAboutCommand());
        registry(new ApplicationExitCommand());
        registry(new ApplicationHelpCommand());
        registry(new ApplicationVersionCommand());
        registry(new ArgumentListCommand());
        registry(new CommandListCommand());
        registry(new SystemInfoCommand());

        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCountCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new TaskBindToProjectCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListByProjectIdCommand());
        registry(new TaskListCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());

        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserViewProfileCommand());
        registry(new UserRegistryCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserChangePasswordCommand());
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public ILoggerService getLoggerService() {
        return loggerService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    private void processArguments(final String[] args) {
        try {
            if (args == null || args.length == 0) return;
            processArgument(args[0]);
            System.exit(0);
        } catch (final Exception e) {
            System.out.println(e.getMessage());
            System.out.println("[FAIL]");
            System.exit(1);
        }
    }

    private void processArgument(final String arg) throws AbstractException {
        final AbstractCommand abstractArgument = commandService.getCommandByArgument(arg);
        if (abstractArgument == null) throw new ArgumentNotSupportedException(arg);
        abstractArgument.execute();
    }

    private void processCommand(final String command) throws AbstractException {
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initLogger() {
        loggerService.info("** WELCOME TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(() ->
                loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **")
        ));
    }

    private void initDemoData() throws AbstractException {
        userService.create("test", "test", "test@test.ru");
        userService.create("user", "user", "user@user.ru");
        userService.create("admin","admin", Role.ADMIN);

        final String user1 = userService.findByLogin("test").getId();
        final String user2 = userService.findByLogin("user").getId();
        final String user3 = userService.findByLogin("admin").getId();

        projectService.add(new Project(user1,"Project_01", "Desc_01_user 1", Status.IN_PROGRESS));
        projectService.add(new Project(user1, "Project_02", "Desc_02_user 1", Status.NOT_STARTED));
        projectService.add(new Project(user1, "Project_03", "Desc_03_user 1", Status.IN_PROGRESS));
        projectService.add(new Project(user1, "Project_04", "Desc_04_user 1", Status.COMPLETED));
        projectService.add(new Project(user2,"Project_01", "Desc_01_user 2", Status.IN_PROGRESS));
        projectService.add(new Project(user2, "Project_02", "Desc_02_user 2", Status.NOT_STARTED));
        projectService.add(new Project(user3, "Project_01", "Desc_01_user 3", Status.IN_PROGRESS));
        projectService.add(new Project(user3, "Project_02", "Desc_02_user 3", Status.COMPLETED));

        final String projectId1 = projectService.findOneByIndex(user1,3).getId();
        final String projectId2 = projectService.findOneByIndex(user2,1).getId();
        final String projectId3 = projectService.findOneByIndex(user3,1).getId();

        taskService.add(new Task(user1, "Task_01", "Desc task 1 user 1", Status.IN_PROGRESS, null));
        taskService.add(new Task(user1, "Task_02", "Desc task 2 user 1", Status.NOT_STARTED, null));
        taskService.add(new Task(user1, "Task_03", "Desc task 3 user 1", Status.COMPLETED, projectId1));
        taskService.add(new Task(user1, "Task_04", "Desc task 4 user 1", Status.NOT_STARTED, projectId1));
        taskService.add(new Task(user2, "Task_01", "Desc task 1 user 2", Status.IN_PROGRESS, projectId2));
        taskService.add(new Task(user2, "Task_02", "Desc task 2 user 2", Status.NOT_STARTED, null));
        taskService.add(new Task(user3, "Task_01", "Desc task 1 user 3", Status.COMPLETED, projectId3));
        taskService.add(new Task(user3, "Task_01", "Desc task 2 user 3", Status.NOT_STARTED, null));
    }

    public void start(final String[] args) throws AbstractException {
        processArguments(args);
        initLogger();
        initDemoData();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

}
