package ru.t1.karimov.tm.api.repository;

import ru.t1.karimov.tm.exception.field.AbstractFieldException;
import ru.t1.karimov.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    Project create(String userId, String name, String description) throws AbstractFieldException;

    Project create(String userId, String name) throws AbstractFieldException;

}
