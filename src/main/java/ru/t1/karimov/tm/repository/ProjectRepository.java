package ru.t1.karimov.tm.repository;

import ru.t1.karimov.tm.api.repository.IProjectRepository;
import ru.t1.karimov.tm.exception.field.AbstractFieldException;
import ru.t1.karimov.tm.model.Project;

public class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @Override
    public Project create(
            final String userId,
            final String name,
            final String description
    ) throws AbstractFieldException {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        return add(project);
    }

    @Override
    public Project create(final String userId, final String name) throws AbstractFieldException {
        final Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        return add(project);
    }

}
